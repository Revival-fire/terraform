region = "us-east-1"

vpc_cidr = "10.0.0.0/16"

enable_dns_support = "true"

enable_dns_hostnames = "true"

enable_classiclink = "false"

enable_classiclink_dns_support = "false"

preferred_number_of_public_subnets = 2

preferred_number_of_private_subnets = 4

environment = "dev"

ami-web = "ami-0decfbe672d951afe"

ami-bastion = "ami-0fa279fdcdbea3693"

ami-nginx = "ami-0729409e168668506"

ami-sonar = "ami-07ec7bcd91adb8030"

keypair = "New_Key"

master-password = "devopspblproject"

master-username = "david"

account_no = "439437829345"

tags = {
  Owner-Email     = "infradev-segun@darey.io"
  Managed-By      = "Terraform"
  Billing-Account = "1234567890"
}